﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyDashboardAccount.Configuration;
using MoneyDashboardAccount.UserInformation;
using MoneyDashboardAccount;
using System.IO.IsolatedStorage;
using System.IO;

namespace MDTest
{
    [TestClass]
    public class MDUnitTests
    {
        // Test file paths and directories are as expected
        [TestMethod]
        public void TestFilesAndPathsGets()
        {
            string TestDirPath = FileLocations.DirPath;
            Assert.AreEqual("MoneyDashboard/PersistInfo", TestDirPath);

            string TestDirFile = FileLocations.DirFile;
            Assert.AreEqual("MDInfo.txt", TestDirFile);

            string TestDirPathFile = FileLocations.DirPathFile;
            Assert.AreEqual("MoneyDashboard/PersistInfo/MDInfo.txt", TestDirPathFile);
        }

        // Test that user information can be set and retrieved from class
        [TestMethod]
        public void TestUserInformationSetsAndGets()
        {
            string TestEmail = "joe@bloggs.com";
            string TestPwd = "abcd1234";

            UserInfo uinfo = new UserInfo();
            uinfo.UserEmail = TestEmail;
            uinfo.UserPwd = TestPwd;

            Assert.AreEqual("joe@bloggs.com", uinfo.UserEmail);
            Assert.AreEqual("abcd1234", uinfo.UserPwd);
        }

        // Test that registration can happen with a valid email address
        [TestMethod]
        public void TestRegistration()
        {
            UserInfo uinfo = new UserInfo();
            uinfo.UserEmail = "john@smith.com";
            uinfo.UserPwd = "zzzz9876";

            Registration testReg = new Registration();
            bool registrationDone = testReg.SaveRegistrationInfo(uinfo);

            Assert.AreEqual(true, registrationDone);
        }

        // Test that registration doesn't happen when an invalid email address is used
        [TestMethod]
        public void TestRegistrationFailedEmailAddress()
        {
            UserInfo uinfo = new UserInfo();
            uinfo.UserEmail = "john.smith.com";
            uinfo.UserPwd = "zzzz9876";

            Registration testReg = new Registration();
            bool registrationDone = testReg.SaveRegistrationInfo(uinfo);

            Assert.AreEqual(false, registrationDone);
        }

        // Test that user info has been persisted
        [TestMethod]
        public void TestGetLoginInfo()
        {
            Login testLogin = new Login();
            UserInfo uinfo = testLogin.GetLoginInfo();
            string TestEmail = uinfo.UserEmail;
            string TestPwd = uinfo.UserPwd;

            Assert.AreEqual("john@smith.com", TestEmail);
            Assert.AreEqual("zzzz9876", TestPwd);
        }

        // Test that the email address is recognised as valid
        [TestMethod]
        public void TestValidEmailAddress()
        {
            bool isValid = ValidateEmail.IsValidEmail("john@smith.com");

            Assert.AreEqual(true, isValid);
        }

        // Test that the email address is recognised as invalid
        [TestMethod]
        public void TestInvalidEmailAddress()
        {
            bool isValid = ValidateEmail.IsValidEmail("john@smithcom");

            Assert.AreEqual(false, isValid);
        }
    }
}
