﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoneyDashboardAccount.Configuration
{
    public static class FileLocations
    {
        private static string _dirPath = "MoneyDashboard/PersistInfo";
        private static string _dirFile = "MDInfo.txt";
        private static string _dirPathFile = "MoneyDashboard/PersistInfo/MDInfo.txt";

        public static string DirPath
        {
            get { return _dirPath; }
        }

        public static string DirFile
        {
            get { return _dirFile; }
        }

        public static string DirPathFile
        {
            get { return _dirPathFile; }
        }
    }
}
