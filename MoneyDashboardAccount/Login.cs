﻿using MoneyDashboardAccount.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using MoneyDashboardAccount.UserInformation;

namespace MoneyDashboardAccount
{
    public class Login
    {
        public UserInfo GetLoginInfo()
        {
            UserInfo uinfo = new UserInfo();

            IsolatedStorageFile isoFile = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly, null, null);

            // Check if directory exists
            if (isoFile.DirectoryExists(FileLocations.DirPath))
            {
                // If directory and file exist, write details to the UserInfo collection
                if (isoFile.FileExists(FileLocations.DirPathFile))
                {
                    IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(FileLocations.DirPathFile, FileMode.Open, isoFile);

                    StreamReader sr = new StreamReader(isfs);
                    uinfo.UserEmail = sr.ReadLine();
                    uinfo.UserPwd = sr.ReadLine();
                    sr.Close();
                }
            }

            return uinfo;
        }
    }
}
