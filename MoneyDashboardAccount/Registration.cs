﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using System.IO;
using MoneyDashboardAccount.UserInformation;
using MoneyDashboardAccount.Configuration;

namespace MoneyDashboardAccount
{
    public class Registration
    {
        public bool SaveRegistrationInfo(UserInfo uinfo)
        {
            if (ValidateEmail.IsValidEmail(uinfo.UserEmail))
            {
                IsolatedStorageFile isoFile = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly, null, null);

                // Check if directory exists
                if (isoFile.DirectoryExists(FileLocations.DirPath))
                {
                    // If directory exists, but file does not, create it with user information passed in
                    if (!isoFile.FileExists(FileLocations.DirPathFile))
                    {
                        IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(FileLocations.DirPathFile, FileMode.Create, isoFile);

                        StreamWriter sw = new StreamWriter(isfs);
                        sw.WriteLine(uinfo.UserEmail);
                        sw.WriteLine(uinfo.UserPwd);
                        sw.Close();
                    }
                    // If directory and file both exist, assume that latest registration should be the one used for persist.
                    // It is entirely conceivable that two people may have registered on the same device but is difficult to determine which one
                    // should be used at which time, hence, the reason for the assumption that last registration should be the default.
                    else
                    {
                        IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(FileLocations.DirPathFile, FileMode.Truncate, isoFile);

                        StreamWriter sw = new StreamWriter(isfs);
                        sw.WriteLine(uinfo.UserEmail);
                        sw.WriteLine(uinfo.UserPwd);
                        sw.Close();
                    }
                }
                // If directory doesn't exist, create it. Correct assumption is that, since the directory doesn't exists, the file will also
                // not exist, so this must be created and written to.
                else
                {
                    isoFile.CreateDirectory(FileLocations.DirPath);

                    IsolatedStorageFileStream isfs = new IsolatedStorageFileStream(FileLocations.DirPathFile, FileMode.Create, isoFile);

                    StreamWriter sw = new StreamWriter(isfs);
                    sw.WriteLine(uinfo.UserEmail);
                    sw.WriteLine(uinfo.UserPwd);
                    sw.Close();
                }

                return true;
            }
            else
            {
                // Handle invalid email address by presenting user with message.
                return false;
            }
        }
    }
}
