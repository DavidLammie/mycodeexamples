﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoneyDashboardAccount.UserInformation
{
    public class UserInfo
    {
        private string _userEmail = string.Empty;
        private string _userPwd = string.Empty;

        public string UserEmail
        {
            get { return _userEmail;}
            set { _userEmail = value; }
        }

        public string UserPwd
        {
            get { return _userPwd; }
            set { _userPwd = value; }
        }
    }
}
