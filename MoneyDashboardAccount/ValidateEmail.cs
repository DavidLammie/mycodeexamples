﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace MoneyDashboardAccount
{
    public static class ValidateEmail
    {
        public static bool IsValidEmail(string emailAddr)
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            Regex regexValid = new Regex(validEmailPattern, RegexOptions.IgnoreCase);

            bool isValid = regexValid.IsMatch(emailAddr);

            return isValid;
        }
    }
}
